package com.example.orderservice;

import com.example.orderservice.Entity.VEntity.TbUserRoleInfo;
import com.example.orderservice.Mapper.UserMapper;
import com.example.orderservice.Mapper.UserRoleInfoMapper;
import lombok.var;
import org.apache.ibatis.annotations.ResultMap;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Collectors;

@SpringBootTest
//@MapperScan("com.example.orderservice.Mapper")
public class UserRoleInfoTest {
    @Autowired
    UserRoleInfoMapper userMapper;

    @Test
    public void test()
    {
        userMapper.getUserRoleInfos().forEach(System.out::println);
        for (TbUserRoleInfo userRoleInfo : userMapper.getUserRoleInfos()) {
            System.out.println(userRoleInfo);
        }
    }

    @Test
    public void testxml()
    {
        //userMapper.getUserRoleInfosByXml().forEach(System.out::println);
        TbUserRoleInfo userRole=userMapper.getUserRoleInfoByUserId(1).get(0);
        userMapper.getUserRoleInfoByUserId(1).forEach(System.out::println);
    }

    @Test
    public void testStream()
    {
        var userRoleList = userMapper.getUserRoleInfos();
        var user1 = userRoleList.stream().filter(p->p.getId()==1).findFirst().get();
        var userselect=userRoleList.stream().filter(p->p.getSex().equals("男")).collect(Collectors.toList());
    }
}
