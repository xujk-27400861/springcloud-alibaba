package com.example.orderservice.service;

import com.example.orderservice.Entity.TbUserrole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
public interface ITbUserroleService extends IService<TbUserrole> {

}
