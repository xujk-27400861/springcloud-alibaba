package com.example.stockservice.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Getter
@Setter
@TableName("tb_ProductStocks")
public class TbProductstocks implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @TableField("ProductID")
    private Long productID;

    /**
     * 库存数量
     */
    @TableField("Inventory")
    private BigDecimal inventory;

    private LocalDateTime createdate;

    private LocalDateTime updatedate;


}
