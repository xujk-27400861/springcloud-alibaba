package com.example.stockservice.service;

import com.example.stockservice.entity.TbProductstocks;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
public interface ITbProductstocksService extends IService<TbProductstocks> {
    int StocksReduce(BigInteger productId, BigDecimal quantity);
}
