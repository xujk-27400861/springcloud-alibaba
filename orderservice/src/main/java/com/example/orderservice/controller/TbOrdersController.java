package com.example.orderservice.controller;


import com.example.orderservice.Entity.TbOrders;
import com.example.orderservice.Entity.VEntity.VOrdersInfo;
import com.example.orderservice.service.ITbOrdersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Api(tags = {"订单管理api"})
@Slf4j
@RestController
@RequestMapping("/tb-orders")
public class TbOrdersController {
    @Autowired
    ITbOrdersService _orderService;

    @ApiOperation(value="保存订单信息")
//    @RequestMapping("/AddOrder")
    @PostMapping("/AddOrder")
    public int AddOrder(VOrdersInfo order)
    {
        try
        {
            return _orderService.AddOrder(order);
        }
        catch (RuntimeException e)
        {
            log.error(e.getMessage().toString());
            return 0;
        }
    }

    @ApiOperation(value="根据订单ID获取订单信息")
    @RequestMapping(value = "/GetOrder",method = RequestMethod.GET)
    public TbOrders GetOrder(long orderId)
    {
        return _orderService.getById(orderId);
    }
}

