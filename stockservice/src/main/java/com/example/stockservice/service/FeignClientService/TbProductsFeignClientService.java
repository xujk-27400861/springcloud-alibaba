package com.example.stockservice.service.FeignClientService;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name="orderservice")
public interface TbProductsFeignClientService {
    /**
     * 查询
     * @return
     */
    @GetMapping("product/provider/get/info")
    public String getServiceInfo();

    /**
     * 查询
     * @param id
     * @return
     */
    @GetMapping("tb-orders/GetOrder?orderId={id}")
    public String selectById(@PathVariable("id") Long id);
}
