package com.example.stockservice.service;

import com.example.stockservice.entity.TbProducts;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
public interface ITbProductsService extends IService<TbProducts> {

}
