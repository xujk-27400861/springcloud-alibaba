package com.example.orderservice.Entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
@Getter
@Setter
@TableName("tb_OrderDetails")
public class TbOrderdetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableField("OrderID")
    private Long orderID;

    /**
     * 商品ID
     */
    @TableField("ProductID")
    private Long productID;

    /**
     * 销售数量
     */
    @TableField("SaleQuantity")
    private BigDecimal saleQuantity;

    /**
     * 销售金额
     */
    @TableField("SaleAmount")
    private BigDecimal saleAmount;

    private LocalDateTime createdate;

    private LocalDateTime updatedate;


}
