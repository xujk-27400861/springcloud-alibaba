package com.example.orderservice.service.impl;

import com.example.orderservice.Entity.TbOrders;
import com.example.orderservice.Entity.VEntity.VOrdersInfo;
import com.example.orderservice.Mapper.TbOrderdetailsMapper;
import com.example.orderservice.Mapper.TbOrdersMapper;
import com.example.orderservice.service.ITbOrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
@Service
public class TbOrdersServiceImpl extends ServiceImpl<TbOrdersMapper, TbOrders> implements ITbOrdersService {
    @Autowired
    TbOrdersMapper _mapper;

    @Autowired
    TbOrderdetailsMapper _detailmapper;

    @Transactional(rollbackFor = Exception.class)
    public int AddOrder(VOrdersInfo order)
    {
        _mapper.insert(order);
        var orderId = order.getOrderID();
        order.details.forEach(p->{p.setOrderID(orderId);_detailmapper.insert(p);});
        return 1;
    }
}
