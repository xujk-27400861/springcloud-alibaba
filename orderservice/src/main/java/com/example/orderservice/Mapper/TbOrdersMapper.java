package com.example.orderservice.Mapper;

import com.example.orderservice.Entity.TbOrders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
@Mapper
public interface TbOrdersMapper extends BaseMapper<TbOrders> {

}
