package com.example.orderservice.Entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
@Getter
@Setter
@TableName("tb_Role")
public class TbRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "roleid", type = IdType.AUTO)
    private Long roleid;

    private String rolename;

    private Long pid;

    private LocalDateTime createdate;

    private LocalDateTime updatedate;


}
