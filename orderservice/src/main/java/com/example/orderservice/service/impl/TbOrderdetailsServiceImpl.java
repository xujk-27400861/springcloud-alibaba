package com.example.orderservice.service.impl;

import com.example.orderservice.Entity.TbOrderdetails;
import com.example.orderservice.Mapper.TbOrderdetailsMapper;
import com.example.orderservice.service.ITbOrderdetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
@Service
public class TbOrderdetailsServiceImpl extends ServiceImpl<TbOrderdetailsMapper, TbOrderdetails> implements ITbOrderdetailsService {

}
