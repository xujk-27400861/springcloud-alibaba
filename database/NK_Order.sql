/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.26 : Database - NK_Order
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`NK_Order` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `NK_Order`;

/*Table structure for table `tb_OrderDetails` */

DROP TABLE IF EXISTS `tb_OrderDetails`;

CREATE TABLE `tb_OrderDetails` (
  `OrderID` bigint(20) NOT NULL COMMENT '订单ID',
  `ProductID` bigint(20) NOT NULL COMMENT '商品ID',
  `SaleQuantity` decimal(18,2) DEFAULT NULL COMMENT '销售数量',
  `SaleAmount` decimal(18,2) DEFAULT NULL COMMENT '销售金额',
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`,`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_OrderDetails` */

insert  into `tb_OrderDetails`(`OrderID`,`ProductID`,`SaleQuantity`,`SaleAmount`,`createdate`,`updatedate`) values (2,1,'10.00','200.00','2021-11-29 13:39:00','2021-11-29 13:39:00');

/*Table structure for table `tb_Orders` */

DROP TABLE IF EXISTS `tb_Orders`;

CREATE TABLE `tb_Orders` (
  `OrderID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `OrderNumber` bigint(20) NOT NULL COMMENT '订单号',
  `ConsumerID` bigint(20) NOT NULL COMMENT '消费者ID',
  `EnterpriseID` bigint(20) NOT NULL COMMENT '商家ID',
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_Orders` */

insert  into `tb_Orders`(`OrderID`,`OrderNumber`,`ConsumerID`,`EnterpriseID`,`createdate`,`updatedate`) values (2,20211129001,1,1,'2021-11-29 13:39:00','2021-11-29 13:39:00');

/*Table structure for table `tb_Role` */

DROP TABLE IF EXISTS `tb_Role`;

CREATE TABLE `tb_Role` (
  `roleid` bigint(20) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(50) NOT NULL,
  `pid` bigint(20) DEFAULT '0',
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_Role` */

/*Table structure for table `tb_User` */

DROP TABLE IF EXISTS `tb_User`;

CREATE TABLE `tb_User` (
  `id` bigint(18) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sex` varchar(20) NOT NULL DEFAULT 'male',
  `pwd` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_User` */

insert  into `tb_User`(`id`,`name`,`sex`,`pwd`,`email`) values (1,'xujk','male','123','test@123.com'),(2,'test1','女','aaaa','1231@qq.com'),(3,'test2','男','aaaa','1232@qq.com'),(4,'test3','女','aaaa','1233@qq.com'),(5,'test4','男','aaaa','1234@qq.com'),(6,'test5','女','aaaa','1235@qq.com'),(7,'test6','男','aaaa','1236@qq.com'),(8,'test7','女','aaaa','1237@qq.com'),(9,'test8','男','aaaa','1238@qq.com'),(10,'test9','女','aaaa','1239@qq.com'),(11,'test10','男','aaaa','12310@qq.com'),(12,'test11','女','aaaa','12311@qq.com'),(13,'test12','男','aaaa','12312@qq.com'),(14,'test13','女','aaaa','12313@qq.com'),(15,'test14','男','aaaa','12314@qq.com');

/*Table structure for table `tb_UserRole` */

DROP TABLE IF EXISTS `tb_UserRole`;

CREATE TABLE `tb_UserRole` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `roleid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_UserRole` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
