package com.example.orderservice.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.orderservice.Entity.VEntity.TbUserRoleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserRoleInfoMapper extends BaseMapper<TbUserRoleInfo> {
    @Select("SELECT * FROM tb_User a\n" +
            "LEFT JOIN tb_UserRole b ON a.`id`=b.`userid`\n" +
            "LEFT JOIN tb_Role c ON b.`roleid`=c.`roleid`")
    List<TbUserRoleInfo> getUserRoleInfos();

    List<TbUserRoleInfo> getUserRoleInfosByXml();

    @Select("SELECT * FROM tb_User a\n" +
            "LEFT JOIN tb_UserRole b ON a.`id`=b.`userid`\n" +
            "LEFT JOIN tb_Role c ON b.`roleid`=c.`roleid` where a.id=#{userId}")
    @Results({
            @Result(property = "userName",  column = "name")
    })
    List<TbUserRoleInfo> getUserRoleInfoByUserId(long userId);
}
