package com.example.stockservice.service.impl;

import com.example.stockservice.entity.TbProductstocks;
import com.example.stockservice.mapper.TbProductstocksMapper;
import com.example.stockservice.service.ITbProductstocksService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Service
public class TbProductstocksServiceImpl extends ServiceImpl<TbProductstocksMapper, TbProductstocks> implements ITbProductstocksService {

    TbProductstocksMapper mapper;

    @Autowired
    public TbProductstocksServiceImpl(TbProductstocksMapper _mapper)
    {
        mapper=_mapper;
    }

    @Transactional(rollbackFor = Exception.class)
    public int StocksReduce(BigInteger productId, BigDecimal quantity)
    {
        return mapper.StocksReduce(productId,quantity);
    }
}
