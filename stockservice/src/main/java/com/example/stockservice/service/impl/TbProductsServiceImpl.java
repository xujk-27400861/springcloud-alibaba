package com.example.stockservice.service.impl;

import com.example.stockservice.entity.TbProducts;
import com.example.stockservice.mapper.TbProductsMapper;
import com.example.stockservice.service.ITbProductsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Service
public class TbProductsServiceImpl extends ServiceImpl<TbProductsMapper, TbProducts> implements ITbProductsService {

}