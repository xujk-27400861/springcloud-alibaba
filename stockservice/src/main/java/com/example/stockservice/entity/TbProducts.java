package com.example.stockservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Getter
@Setter
@TableName("tb_Products")
public class TbProducts implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ProductID", type = IdType.AUTO)
    private Long productID;

    /**
     * 商品名称
     */
    @TableField("ProductName")
    private String productName;

    /**
     * 品牌
     */
    @TableField("Brand")
    private String brand;

    /**
     * 规格
     */
    @TableField("Specification")
    private String specification;

    /**
     * 备注
     */
    @TableField("Rmarks")
    private String rmarks;

    private LocalDateTime createdate;

    private LocalDateTime updatedate;


}
