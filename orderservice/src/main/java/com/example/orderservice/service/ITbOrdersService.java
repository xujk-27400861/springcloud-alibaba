package com.example.orderservice.service;

import com.example.orderservice.Entity.TbOrders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.orderservice.Entity.VEntity.VOrdersInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
public interface ITbOrdersService extends IService<TbOrders> {
    int AddOrder(VOrdersInfo order);
}
