package com.example.orderservice.ServiceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.orderservice.Entity.User;
import com.example.orderservice.Mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl {
    @Autowired
    UserMapper userMapper;
    //查询全部
    public List<User> queryAll() {
        return userMapper.selectList(null);
    }

    //添加一条数据
    public int add(User user) {
        return userMapper.insert(user);
    }
    //添加多条数据
    public void add(List<User> users) {
        for (User user : users) {
            add(user);
        }
    }
    //根据id查询
    public User queryById(User user) {
        return userMapper.selectById(user.getId());
    }
    //根据name模糊查询
    public List<User> queryByName(String name) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.like("name", name); //参数为表中的列名，要查询的条件 相当于 WHERE name LIKE  %name%
        return userMapper.selectList(userQueryWrapper);
    }
    //精确查询
    public List<User> queryByName2(String name) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("name", name);//参数为表中的列名，要查询的条件 相当于 WHERE name = name
        return userMapper.selectList(userQueryWrapper);
    }
    //精确查询
    public List<User> queryByNameMap(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return userMapper.selectByMap(map);
    }
    //批量查询
    public List<User> queryByIds() {
        List<Integer> idList = new ArrayList<>();
        idList.add(10);
        idList.add(11);
        return userMapper.selectBatchIds(idList);
    }
    //计数
    public int count() {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        return userMapper.selectCount(userQueryWrapper);
    }

    //region update
    // 根据条件更新
    public void changeBy(User user, String column, Object val) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq(column, val);
        int num = userMapper.update(user, userQueryWrapper);
        System.out.println("影响行数：" + num);
    }

    // 通过ID修改信息
    public void changeUserById(User user) {
        int num = userMapper.updateById(user);
        System.out.println("影响行数：" + num);
    }
    //endregion

    //region delete
    public int deleteById(User user) {
        return userMapper.deleteById(user.getId());
    }
    public void deleteBy(String column, Object val) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq(column, val);
        int num = userMapper.delete(userQueryWrapper);
        System.out.println("影响行数：" + num);
    }
    public void delete(Map<String, Object> map) {
        userMapper.deleteByMap(map);
    }

    public void deleteByIds() {
        List<Integer> idList = new ArrayList<>();
        idList.add(10);
        idList.add(11);
        int num = userMapper.deleteBatchIds(idList);
        System.out.println("影响行数：" + num);
    }
    //endregion
}
