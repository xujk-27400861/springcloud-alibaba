package com.example.stockservice.mapper;

import com.example.stockservice.entity.TbProducts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Mapper
public interface TbProductsMapper extends BaseMapper<TbProducts> {

}
