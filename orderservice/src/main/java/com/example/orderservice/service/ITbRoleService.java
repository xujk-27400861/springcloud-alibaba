package com.example.orderservice.service;

import com.example.orderservice.Entity.TbRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
public interface ITbRoleService extends IService<TbRole> {

}
