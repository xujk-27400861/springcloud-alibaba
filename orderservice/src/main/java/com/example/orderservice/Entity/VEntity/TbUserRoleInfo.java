package com.example.orderservice.Entity.VEntity;

import lombok.Data;

@Data
public class TbUserRoleInfo extends com.example.orderservice.Entity.TbUser
{
    private int roleid;
    private String rolename;
    private String userName;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String username) {
        this.userName = username;
    }
}