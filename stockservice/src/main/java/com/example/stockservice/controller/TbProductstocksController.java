package com.example.stockservice.controller;


import com.example.stockservice.service.ITbProductstocksService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Api(tags = {"库存管理api"})
@RestController
@RequestMapping("/tb-productstocks")
public class TbProductstocksController {
    ITbProductstocksService service;

    @Autowired
    public TbProductstocksController(ITbProductstocksService _service)
    {
        service=_service;
    }

    @ApiOperation(value="扣减库存")
    @PostMapping("/StocksReduce")
    public int StocksReduce(BigInteger productId, BigDecimal quantity)
    {
        try
        {
            return service.StocksReduce(productId,quantity);
        }
        catch (RuntimeException e)
        {
            return 0;
        }
    }
}

