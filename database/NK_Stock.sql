/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.26 : Database - NK_Stock
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`NK_Stock` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `NK_Stock`;

/*Table structure for table `tb_ProductStocks` */

DROP TABLE IF EXISTS `tb_ProductStocks`;

CREATE TABLE `tb_ProductStocks` (
  `ProductID` bigint(20) NOT NULL COMMENT '商品ID',
  `Inventory` decimal(18,0) NOT NULL DEFAULT '0' COMMENT '库存数量',
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_ProductStocks` */

insert  into `tb_ProductStocks`(`ProductID`,`Inventory`,`createdate`,`updatedate`) values (1,'97','2021-11-30 14:38:18','2021-11-30 15:31:06'),(2,'200','2021-11-30 14:38:24','2021-11-30 14:38:24');

/*Table structure for table `tb_Products` */

DROP TABLE IF EXISTS `tb_Products`;

CREATE TABLE `tb_Products` (
  `ProductID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(100) NOT NULL COMMENT '商品名称',
  `Brand` varchar(150) DEFAULT NULL COMMENT '品牌',
  `Specification` varchar(100) DEFAULT NULL COMMENT '规格',
  `Rmarks` varchar(200) DEFAULT NULL COMMENT '备注',
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_Products` */

insert  into `tb_Products`(`ProductID`,`ProductName`,`Brand`,`Specification`,`Rmarks`,`createdate`,`updatedate`) values (1,'运动裤','耐克','34','耐克运动裤34码','2021-11-30 14:37:30','2021-11-30 14:37:30'),(2,'运动鞋D109','李宁','43','李宁运动鞋','2021-11-30 14:38:08','2021-11-30 14:38:08');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
