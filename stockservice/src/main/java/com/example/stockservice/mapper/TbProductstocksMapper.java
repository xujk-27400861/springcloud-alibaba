package com.example.stockservice.mapper;

import com.example.stockservice.entity.TbProductstocks;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujk
 * @since 2021-11-23
 */
@Mapper
public interface TbProductstocksMapper extends BaseMapper<TbProductstocks> {
    @Update("UPDATE \n" +
            "  NK_Stock.tb_ProductStocks \n" +
            "SET\n" +
            "  Inventory = Inventory-#{quantity}\n" +
            "WHERE ProductID = #{productId} ;")
    int StocksReduce(@Param("productId") BigInteger productId, @Param("quantity") BigDecimal quantity);
}
