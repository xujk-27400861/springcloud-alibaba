package com.example.orderservice.Mapper;

import com.example.orderservice.Entity.TbUserrole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
@Mapper
public interface TbUserroleMapper extends BaseMapper<TbUserrole> {

}
