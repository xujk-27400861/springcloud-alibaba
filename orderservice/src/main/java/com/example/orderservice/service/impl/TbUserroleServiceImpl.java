package com.example.orderservice.service.impl;

import com.example.orderservice.Entity.TbUserrole;
import com.example.orderservice.Mapper.TbUserroleMapper;
import com.example.orderservice.service.ITbUserroleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
@Service
public class TbUserroleServiceImpl extends ServiceImpl<TbUserroleMapper, TbUserrole> implements ITbUserroleService {

}
