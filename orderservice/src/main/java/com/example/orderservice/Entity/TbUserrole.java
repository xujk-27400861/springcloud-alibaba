package com.example.orderservice.Entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
@Getter
@Setter
@TableName("tb_UserRole")
public class TbUserrole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long userid;

    private Long roleid;


}
