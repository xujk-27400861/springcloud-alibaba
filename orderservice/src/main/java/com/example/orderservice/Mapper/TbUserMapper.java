package com.example.orderservice.Mapper;

import com.example.orderservice.Entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujk
 * @since 2021-11-12
 */
@Mapper
public interface TbUserMapper extends BaseMapper<TbUser> {

}
