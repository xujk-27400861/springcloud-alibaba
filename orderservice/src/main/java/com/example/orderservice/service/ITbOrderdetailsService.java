package com.example.orderservice.service;

import com.example.orderservice.Entity.TbOrderdetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
public interface ITbOrderdetailsService extends IService<TbOrderdetails> {

}
