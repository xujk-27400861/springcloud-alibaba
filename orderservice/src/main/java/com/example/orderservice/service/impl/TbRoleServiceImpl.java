package com.example.orderservice.service.impl;

import com.example.orderservice.Entity.TbRole;
import com.example.orderservice.Mapper.TbRoleMapper;
import com.example.orderservice.service.ITbRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-14
 */
@Service
public class TbRoleServiceImpl extends ServiceImpl<TbRoleMapper, TbRole> implements ITbRoleService {

}
