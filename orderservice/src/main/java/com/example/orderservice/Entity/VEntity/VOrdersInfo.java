package com.example.orderservice.Entity.VEntity;

import lombok.Data;

import java.util.List;

@Data
public class VOrdersInfo extends com.example.orderservice.Entity.TbOrders {
    public List<com.example.orderservice.Entity.TbOrderdetails> details;
}
