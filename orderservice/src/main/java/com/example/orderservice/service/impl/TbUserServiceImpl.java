package com.example.orderservice.service.impl;

import com.example.orderservice.Entity.TbUser;
import com.example.orderservice.Mapper.TbUserMapper;
import com.example.orderservice.service.ITbUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujk
 * @since 2021-11-12
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements ITbUserService {

}
