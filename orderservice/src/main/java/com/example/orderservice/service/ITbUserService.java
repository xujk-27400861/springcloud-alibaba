package com.example.orderservice.service;

import com.example.orderservice.Entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujk
 * @since 2021-11-12
 */
public interface ITbUserService extends IService<TbUser> {

}
