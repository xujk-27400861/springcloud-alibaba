package com.example.orderservice.Entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.beans.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.Data;
import org.apache.ibatis.annotations.ResultMap;

/**
 * <p>
 * 
 * </p>
 *
 * @author xujk
 * @since 2021-11-24
 */
@Getter
@Setter
@TableName("tb_Orders")
public class TbOrders implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId(value = "OrderID", type = IdType.AUTO)
    private Long orderID;

    /**
     * 订单号
     */
    @TableField("OrderNumber")
    private Long orderNumber;

    /**
     * 消费者ID
     */
    @TableField("ConsumerID")
    private Long consumerID;

    /**
     * 商家ID
     */
    @TableField("EnterpriseID")
    private Long enterpriseID;

    private LocalDateTime createdate;

    private LocalDateTime updatedate;


}
