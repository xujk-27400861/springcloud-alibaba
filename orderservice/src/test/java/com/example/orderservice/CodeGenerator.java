package com.example.orderservice;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.IFill;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.fill.Property;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

/**
 * 相关依赖
 * 1.mybatis-plus-boot-starter
 * 2.mybatis-plus-generator
 * 3.velocity-engine-core
 * url:https://www.cnblogs.com/sqhuang/p/15315156.html
 * url:https://my.oschina.net/csxk/blog/5270262
 */

@SpringBootTest(classes = CodeGenerator.class)
public class CodeGenerator {

//    @Autowired
//    private DataSource dataSource;

    //需要配置
    private final String author = "xujk";                         //作者
    private final String packageName = "com.example.orderservice";            //项目主路径包名(com.boot01.test01)
    private final String dbTables = "tb_Orders,tb_OrderDetails";                          //需要生成的表名
    private final Boolean enableSwagger = false;                 //是否开启Swagger

    @Test
    public void main() {

        // 1.数据源配置
        //DataSourceConfig.Builder dataSourceConfigBuilder = new DataSourceConfig.Builder(dataSource);
        DataSourceConfig dataSourceConfigBuilder = new DataSourceConfig.Builder("jdbc:mysql://192.168.231.138:3306/NK_Order","root","root")
                .dbQuery(new MySqlQuery())
                .schema("NK_Order")
                .typeConvert(new MySqlTypeConvert())
                .keyWordsHandler(new MySqlKeyWordsHandler())
                .build();

        // 2.全局配置
        GlobalConfig.Builder globalConfigBuilder = new GlobalConfig.Builder();
        // 代码生成目录
        String projectPath = System.getProperty("user.dir");
        globalConfigBuilder.outputDir(projectPath + "/src/main/java");
        // 作者
        globalConfigBuilder.author(author);
        // 结束时是否打开文件夹
        globalConfigBuilder.disableOpenDir();

        // 实体属性Swagger2注解
        if (enableSwagger){
            globalConfigBuilder.enableSwagger();
        }

        // 3.包配置
        PackageConfig.Builder packageConfigBuilder = new PackageConfig.Builder();
        packageConfigBuilder.parent(packageName);           //项目包名

        //都有默认值   配置实体、mapper、service、controller的包名
        packageConfigBuilder.entity("Entity");
        packageConfigBuilder.mapper("Mapper");
        //packageConfigBuilder.service("ServiceImpl");
//        packageConfigBuilder.controller("controller");

        // 4.策略配置
        StrategyConfig.Builder strategyConfigBuilder = new StrategyConfig.Builder();
        // 设置需要映射的表名  用逗号分割
        strategyConfigBuilder.addInclude(dbTables.split(","));
        // 下划线转驼峰
        strategyConfigBuilder.entityBuilder().naming(NamingStrategy.underline_to_camel);
        strategyConfigBuilder.entityBuilder().columnNaming(NamingStrategy.underline_to_camel);
        // entity的Lombok
        strategyConfigBuilder.entityBuilder().enableLombok();
        // 逻辑删除
        strategyConfigBuilder.entityBuilder().logicDeleteColumnName("deleted");
        strategyConfigBuilder.entityBuilder().logicDeletePropertyName("deleted");
        // 自动填充
        // 创建时间
        IFill gmtCreate = new Property("gmt_create", FieldFill.INSERT);
        // 更新时间
        IFill gmtModified = new Property("gmt_modified", FieldFill.INSERT_UPDATE);
        strategyConfigBuilder.entityBuilder().addTableFills(gmtCreate, gmtModified);
        // 乐观锁
        strategyConfigBuilder.entityBuilder().versionColumnName("version");
        strategyConfigBuilder.entityBuilder().versionPropertyName("version");
        // 使用RestController
        strategyConfigBuilder.controllerBuilder().enableRestStyle();
        // 将请求地址转换为驼峰命名，如 http://localhost:8080/hello_id_2
        strategyConfigBuilder.controllerBuilder().enableHyphenStyle();

        /**mapper配置*/
        strategyConfigBuilder.mapperBuilder()
        //开启 @Mapper 注解
        .enableMapperAnnotation();

        // 创建代码生成器对象，加载配置   对应1.2.3.4步
        AutoGenerator autoGenerator = new AutoGenerator(dataSourceConfigBuilder);
        autoGenerator.global(globalConfigBuilder.build());
        autoGenerator.packageInfo(packageConfigBuilder.build());
        autoGenerator.strategy(strategyConfigBuilder.build());

        // 执行
        autoGenerator.execute();
    }
}